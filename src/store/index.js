import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  actions: {},
  modules: {},
  state: {
    drawerShow: false,
    colorShow: false,
    devMode: false,
    baseURL: process.env.VUE_APP_MIRROR_URL
  },
  mutations: {
    toggleDrawer(state) {
      state.drawerShow = !state.drawerShow;
    },
    updateDrawer(state, val) {
      state.drawerShow = val;
    },
    updateColor(state, val) {
      state.colorShow = val;
    },
    updateDevMode(state, val) {
      state.devMode = val;
    },
    updateBaseURL(state, val) {
      state.baseURL = "https://" + val;
    }
  }
});
