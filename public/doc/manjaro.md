## Manjaro 软件仓库镜像使用帮助

编辑 `/etc/pacman.d/mirrorlist`， 在文件的最顶端添加：

```
Server = https://mirrors.uestc.cn/manjaro/stable/$repo/$arch
```

更新软件包缓存： `sudo pacman -Syy`

`pacman-mirrors` 等命令会自动生成镜像列表并覆盖你的改动，请避免使用
