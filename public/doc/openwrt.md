## OpenWRT/LEDE 镜像使用帮助

一般情况下，下载来自 downloads.openwrt.org 或 downloads.lede-project.org 的文件时，将 URL 中的这部分域名替换为 mirrors.uestc.cn/openwrt 即可。

如要使用本镜像作为 OpenWRT/LEDE 系统 opkg 软件仓库，SSH 登录路由器编辑 /etc/opkg/distfeeds.conf 文件，同样按照上面的方法替换域名即可。可以使用如下命令操作：
```shell

sed -i 's_downloads\.lede-project\.org_mirrors.uestc.cn/openwrt_' /etc/opkg/distfeeds.conf
sed -i 's_downloads\.openwrt\.org_mirrors.uestc.cn/openwrt_' /etc/opkg/distfeeds.conf
```
之后运行 opkg update 更新软件索引，注意检查是否出现错误。
